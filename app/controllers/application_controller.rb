class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :validate_ip

  private

  def validate_ip
    if signed_in?
      head 403 unless current_user.allowed_for_ip?(request.ip)
    end
  end
end
