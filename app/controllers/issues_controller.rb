class IssuesController < ApplicationController
  before_action :set_issue, only: [:show, :edit, :take_ownership, :fire]
  before_action :authenticate_user!, only: [:index, :take_ownership, :fire]

  def index
    @filter = params[:filter]
    @issues = Issue.by_filter(@filter).order(id: :desc)
  end

  def show
    @comment = Comment.new(issue: @issue)
  end

  def new
    @issue = Issue.new
  end

  def create
    @issue = Issue.new(issue_params)

    respond_to do |format|
      if @issue.save
        format.html { redirect_to @issue, notice: 'Issue was successfully created.' }
      else
        format.html { render action: 'new' }
      end
    end
  end

  def take_ownership
    @issue.take_ownership!(current_user)
    p @issue
    respond_to do |format|
      format.html { redirect_to @issue, notice: 'Issue was successfully updated.' }
    end
  end

  def fire
    respond_to do |format|
      if @issue.owned_by?(current_user)
        @issue.fire!(params[:event])
        format.html { redirect_to @issue, notice: 'Issue was successfully updated.' }
      else
        format.html { render status: :not_found }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_issue
      @issue = Issue.find(params[:id] || params[:issue_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def issue_params
      params[:issue].permit(:customer_name, :customer_email, :content, :department_id)
    end
end
