class CommentsController < ApplicationController
  before_action :set_issue, only: [:create]

  def create
    @comment = @issue.comments.new(comment_params)
    @comment.user = current_user

    respond_to do |format|
      if @comment.save
        format.html { redirect_to @issue }
      else
        format.html { render 'issues/show' }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_issue
      @issue = Issue.find(params[:issue_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params[:comment].permit(:content)
    end
end
