class IssueMailer < ActionMailer::Base
  default from: "from@example.com"

  def new_comment_email(comment)
    @comment = comment
    mail(to: comment.issue.customer_email, subject: "New comment on issue ##{comment.issue.id}")
  end
end
