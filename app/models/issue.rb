class Issue < ActiveRecord::Base
  include AASM

  belongs_to :department
  has_many :comments

  validates :customer_name, :content, presence: true
  validates :customer_email, format: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  validates :state, inclusion: { in: %w(waiting_for_staff_response waiting_for_customer on_hold cancelled completed) }

  belongs_to :user
  belongs_to :department

  scope :unassigned, -> { where('issues.user_id is null') }
  scope :by_user, -> (user) { where(user_id: user) }

  scope :open, -> { where(state: [:waiting_for_staff_response, :waiting_for_customer]) }
  scope :closed, -> { where(state: [:cancelled, :completed]) }

  def open?
    %w(waiting_for_staff_response waiting_for_customer).include?(state)
  end

  def closed?
    %w(cancelled completed).include?(state)
  end

  def update_state_by_last_comment
    if open? && (last_comment = comments.last)
      update_attribute(:state, last_comment.from_customer? ? :waiting_for_staff_response : :waiting_for_customer)
    end
  end

  def self.by_filter(filter)
    case filter
    when 'new_unassigned_items'
      open.unassigned
    when 'open_tickets'
      open
    when 'on_hold_tickets'
      on_hold
    when 'closed_tickets'
      closed
    else
      where(nil)
    end
  end

  aasm column: :state do 
    state :waiting_for_staff_response, initial: true
    state :waiting_for_customer 
    state :on_hold 
    state :cancelled 
    state :completed

    event :cancel do
      transitions from: [:waiting_for_staff_response, :waiting_for_customer, :on_hold], to: :cancelled
    end

    event :complete do
      transitions from: [:waiting_for_staff_response, :waiting_for_customer, :on_hold], to: :completed
    end

    event :hold do
      transitions from: [:waiting_for_staff_response, :waiting_for_customer], to: :on_hold
    end

    event :unhold do
      after do
        update_state_by_last_comment
      end
      transitions from: :on_hold, to: :waiting_for_staff_response
    end
  end

  def take_ownership!(user)
    update_attribute(:user_id, user.id)
  end

  def fire!(event)
    if %w(cancel complete hold unhold).include? event
      send("#{event}!")
    end
  end

  def owned_by?(user)
    self.user.try(:id) == user.id
  end

end
