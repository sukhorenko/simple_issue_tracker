class Comment < ActiveRecord::Base
  belongs_to :issue
  belongs_to :user

  validates :content, presence: true

  after_save :update_issue_state
  after_save :notify_customer

  def from_customer?
    user.blank?
  end

  private

  def update_issue_state
    issue.update_state_by_last_comment
  end

  def notify_customer
    IssueMailer.new_comment_email(self).deliver
  end
end
