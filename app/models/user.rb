class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :rememberable, :trackable, :validatable

  validate :validate_ip_address

  def allowed_for_ip?(ip)
    if allowed_ip_address.present?
      from, to = allowed_ip_address.split('-')
      range = if to.present?
                IPAddr.new(from)..IPAddr.new(to)
              else
                IPAddr.new(from).to_range
              end
      range.include?(ip)
    else
      true
    end
  end

  private

  def validate_ip_address
    if allowed_ip_address.present?
      from, to = allowed_ip_address.split('-')
      begin
        IPAddr.new(from)
      rescue
        errors.add(:allowed_ip_address, 'Invalid address')
      end
      begin
        ip_to = IPAddr.new(to)
      rescue
        errors.add(:allowed_ip_address, 'Invalid address')
      end
      if ip_to.present? && (ip_from.to_range > 1 || ip_from.to_range > 1)
        errors.add(:allowed_ip_address, 'Invalid address')
      end
    end
  end
end
