module CommentsHelper
  def describe_comment_author(comment)
    describe_user(comment.user) || comment.issue.customer_name
  end
end
