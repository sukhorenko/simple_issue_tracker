module IssuesHelper
  def issue_filter_options
    options_for_select(%w{new_unassigned_items open_tickets on_hold_tickets closed_tickets}.
                       inject([]) { |r, i| r << [i.humanize, i, {'data-url' => issues_path(filter: i)}] }, @filter)
  end
end
