# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
SimpleIssueTracker::Application.config.secret_key_base = 'e5a6cd9af57bc182be5a1c8b5bc6a37a7a2a402a13c9c012556bc4d089d51ecd7b8b7c6f1078d9b2da18efe7fef745b922ab88b3fa48fd59e7cd877f74799d23'
