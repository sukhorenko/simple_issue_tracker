# Be sure to restart your server when you modify this file.

SimpleIssueTracker::Application.config.session_store :cookie_store, key: '_simple_issue_tracker_session'
