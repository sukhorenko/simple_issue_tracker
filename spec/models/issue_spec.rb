require 'spec_helper'

describe Issue do
  describe 'take_ownership!' do
    it 'takes ownership of issue' do
      issue = create(:issue)
      issue.take_ownership!(user = create(:user))
      issue.reload
      expect(issue.owned_by?(user)).to be_true
    end
  end
end
