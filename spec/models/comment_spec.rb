require 'spec_helper'

describe Comment do
  context 'staff comments' do
    context '"Waiting for staff response" issue' do
      it 'changes state to "Waiting for customer"' do
        issue = create(:issue, state: 'waiting_for_staff_response')
        create(:comment, issue: issue, user: create(:user))
        issue.reload
        expect(issue).to be_waiting_for_customer
      end
    end

    shared_examples 'comment does not affect the state' do
      it 'does not change the state' do
        issue = create(:issue, state: state)
        create(:comment, issue: issue, user: create(:user))
        issue.reload
        expect(issue.state).to eq state
      end
    end

    Issue.aasm.states.each do |s|
      next if s.name == :waiting_for_staff_response
      context "#{s.name} issue" do
        it_behaves_like 'comment does not affect the state' do
          let(:state) { s.name.to_s }
        end
      end
    end
  end

  context 'client comments' do
    context '"Waiting for customer" issue' do
      it 'changes state to "Waiting for staff response"' do
        issue = create(:issue, state: 'waiting_for_staff_response')
        create(:comment, issue: issue)
        issue.reload
        expect(issue).to be_waiting_for_staff_response
      end
    end

    shared_examples 'comment does not affect the state' do
      it 'does not change the state' do
        issue = create(:issue, state: state)
        create(:comment, issue: issue)
        issue.reload
        expect(issue.state).to eq state
      end
    end

    Issue.aasm.states.each do |s|
      next if s.name == :waiting_for_customer
      context "#{s.name} issue" do
        it_behaves_like 'comment does not affect the state' do
          let(:state) { s.name.to_s }
        end
      end
    end

  end
end
