require 'spec_helper'

feature 'Issues' do
  scenario 'Register a new Issue' do
    expect {
      visit new_issue_path

      expect(current_path).to eq new_issue_path
      fill_in 'Customer name', with: Faker::Name.name
      fill_in 'Customer email', with: Faker::Internet.email
      fill_in 'Description', with: Faker::Lorem.paragraph
      click_on 'Submit'
      expect(current_path).to eq issue_path(Issue.last)
    }.to change(Issue, :count).by(1)
  end

  scenario 'Post a new Comment on Issue' do
    @issue = create(:issue)

    expect {
      visit issue_path(@issue)

      expect(current_path).to eq issue_path(@issue)
      fill_in 'Content', with: Faker::Lorem.paragraph
      click_on 'Submit'
      expect(current_path).to eq issue_path(@issue)
      expect(open_last_email).to have_body_text(issue_url(@issue, host: 'localhost:3000'))
    }.to change(@issue.comments, :count).by(1)
  end

  context 'staff member' do
    before do
      @user = create(:user)
      login @user
    end

    scenario 'Take ownership' do
      issue = create(:issue)

      visit issue_path(issue)
      expect(current_path).to eq issue_path(issue)
      click_on 'Take ownership'
      expect(page.find('#owner_container')).to have_content(@user.email)
      # save_and_open_page
    end

    scenario 'Fire Issue (Hold)' do
      issue = create(:issue, user: @user)

      visit issue_path(issue)
      expect(current_path).to eq issue_path(issue)
      click_on 'Hold'
      expect(page.find('#state_container')).to have_content('On hold')
    end
  end
end

