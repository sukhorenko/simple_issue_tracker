# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :issue do
    customer_name { Faker::Name.name }
    customer_email { Faker::Internet.email }
    content { Faker::Lorem.paragraph }
  end
end
