class CreateIssues < ActiveRecord::Migration
  def change
    create_table :issues do |t|
      t.string :customer_name
      t.string :customer_email
      t.text :content
      t.string :state
      t.string :number, index: true
      t.references :department, index: true
      t.references :user, index: true

      t.timestamps
    end
  end
end
