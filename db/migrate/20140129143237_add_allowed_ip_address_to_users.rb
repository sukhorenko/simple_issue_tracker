class AddAllowedIpAddressToUsers < ActiveRecord::Migration
  def change
    add_column :users, :allowed_ip_address, :string
  end
end
